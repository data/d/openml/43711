# OpenML dataset: Countries-of-the-World

https://www.openml.org/d/43711

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
World fact sheet, fun to link with other datasets.
Content
Information on population, region, area size, infant mortality and more.
Acknowledgements
Source: All these data sets are made up of data from the US government. Generally they are free to use if you use the data in the US. If you are outside of the US, you may need to contact the US Govt to ask.
Data from the World Factbook is public domain. The website says "The World Factbook is in the public domain and may be used freely by anyone at anytime without seeking permission."    
https://www.cia.gov/library/publications/the-world-factbook/docs/faqs.html   
Inspiration
When making visualisations related to countries, sometimes it is interesting to group them by attributes such as region, or weigh their importance by population, GDP or other variables.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43711) of an [OpenML dataset](https://www.openml.org/d/43711). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43711/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43711/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43711/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

